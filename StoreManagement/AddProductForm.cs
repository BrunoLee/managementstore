﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement
{
    public partial class AddProductForm : Form
    {
        MainForm _mainForm;
        SQLiteDataAdapter _adp = new SQLiteDataAdapter();
        int rowSelected = 0;
        string mConnectionString = "Data Source = MyDatabase.sqlite;Version=3;";
        string mDatetimePattern = "dd/MM/yyyy HH:mm:ss";
        string mUser = string.Empty;
        public AddProductForm(MainForm mainForm, string user)
        {
            InitializeComponent();
            _mainForm = mainForm;
            mUser = user;
            if (mUser == "admin")
            {
                lbl_tenhangmoi.Enabled = true;
                txt_tenhangmoi.Enabled = true;
                btn_themtenhang.Enabled = true;
                btn_xoatenhang.Enabled = true;
            }
            this.WindowState = FormWindowState.Maximized;
            txt_tenhangmoi.AutoCompleteMode = AutoCompleteMode.Suggest;
            txt_tenhangmoi.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        private void btn_backmain_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
            _mainForm.Show();
        }

        private void txt_mahang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
                {
                    c.Open();
                    dgv_sanpham.DataSource = null;
                    dgv_sanpham.Rows.Clear();
                    try
                    {
                        DataTable dt = new DataTable();
                        _adp = new SQLiteDataAdapter("select * from tbl_product where serial =" + "'" + txt_mahang.Text + "'", c);
                        _adp.Fill(dt);
                        dgv_sanpham.DataSource = dt;
                        string ngaynhap = dgv_sanpham.Rows[0].Cells[2].Value.ToString();
                        DateTime datetime = DateTime.Now;
                        txt_mahang.Text = dgv_sanpham.Rows[0].Cells[0].Value.ToString();
                        cb_tenhang.Text = dgv_sanpham.Rows[0].Cells[1].Value.ToString();
                        DateTime.TryParseExact(ngaynhap, mDatetimePattern, null, DateTimeStyles.None, out datetime);
                        dtp_ngaynhap.Value = datetime;
                        decimal soluong = 0;
                        string ssoluong = dgv_sanpham.Rows[0].Cells[3].Value.ToString();
                        decimal.TryParse(ssoluong, out soluong);
                        num_soluong.Value = soluong;
                        txt_ghichu.Text = dgv_sanpham.Rows[0].Cells[4].Value.ToString();

                        txt_ghichu.Focus();
                        txt_ghichu.SelectAll();
                    }
                    catch (Exception ex)
                    {
                        cb_tenhang.Focus();
                        //txt_tenhang.SelectAll();
                    }
                }
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            if (txt_mahang.Text != "" && txt_ghichu.Text != "")
            {
                string strInsert = string.Format("Insert into tbl_product(serial,name,receiveddate,quantity,note) VALUES('{0}','{1}','{2}','{3}','{4}')", txt_mahang.Text, cb_tenhang.Text, dtp_ngaynhap.Value.ToString(mDatetimePattern), num_soluong.Value.ToString(), txt_ghichu.Text);

                using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
                {
                    c.Open();
                    try
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(strInsert, c))
                        {
                            cmd.ExecuteNonQuery();
                            this.dgv_sanphammoi.Rows.Add(dgv_sanphammoi.Rows.Count, txt_mahang.Text, cb_tenhang.Text, dtp_ngaynhap.Value.ToString(mDatetimePattern), num_soluong.Value.ToString(), txt_ghichu.Text);
                            txt_mahang.Focus();
                            txt_mahang.Text = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        using (SQLiteCommand UpdateCommand = new SQLiteCommand("Update tbl_product set serial=@_serial,name=@_name,receiveddate =@_receiveddate,quantity =@_quantity" +
                            ",note =@_note" + " where serial=@_serial", c))
                        {
                            UpdateCommand.Parameters.Add("@_serial", DbType.String).Value = txt_mahang.Text;
                            UpdateCommand.Parameters.Add("@_name", DbType.String).Value = cb_tenhang.Text;
                            UpdateCommand.Parameters.Add("@_receiveddate", DbType.String).Value = dtp_ngaynhap.Value.ToString(mDatetimePattern);
                            UpdateCommand.Parameters.Add("@_quantity", DbType.String).Value = num_soluong.Value.ToString();
                            UpdateCommand.Parameters.Add("@_note", DbType.String).Value = txt_ghichu.Text;
                            UpdateCommand.ExecuteNonQuery();
                            foreach (DataGridViewRow row in dgv_sanphammoi.Rows)
                            {
                                if (row.Cells[1].Value.ToString() == txt_mahang.Text)
                                {
                                    rowSelected = row.Index;
                                }
                            }
                            if (rowSelected > -1 && dgv_sanphammoi.Rows.Count > 0)
                            {
                                dgv_sanphammoi.Rows.RemoveAt(rowSelected);
                            }
                            this.dgv_sanphammoi.Rows.Insert(rowSelected, rowSelected, txt_mahang.Text, cb_tenhang.Text, dtp_ngaynhap.Value.ToString(mDatetimePattern), num_soluong.Value.ToString(), txt_ghichu.Text);
                            txt_mahang.Focus();
                            txt_mahang.Text = "";
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Hãy nhập đủ thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                string str = string.Format("DELETE FROM tbl_product where serial='{0}'", txt_mahang.Text);
                try
                {
                    _adp.DeleteCommand = c.CreateCommand();
                    _adp.DeleteCommand.CommandText = str;
                    _adp.DeleteCommand.ExecuteNonQuery();
                    dgv_sanphammoi.Rows.RemoveAt(rowSelected);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Thông báo");
                }

                ClearAllInfo();
            }
        }

        private void ClearAllInfo()
        {
            txt_mahang.Clear();
            cb_tenhang.SelectedIndex = -1;
            dtp_ngaynhap.Value = DateTime.Now;
            num_soluong.Value = 1;
            txt_ghichu.Clear();
            txt_mahang.Focus();
        }

        private void dgv_sanphammoi_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                rowSelected = e.RowIndex;
                if (e.RowIndex != -1 && dgv_sanphammoi.Rows[rowSelected].Cells["Mahang"].Value != null)
                {
                    using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
                    {
                        c.Open();
                        dgv_sanphammoi.ClearSelection();
                        this.dgv_sanphammoi.Rows[rowSelected].Selected = true;
                        try
                        {
                            DataTable dt = new DataTable();
                            _adp = new SQLiteDataAdapter("select * from tbl_product where serial =" + "'" + dgv_sanphammoi.Rows[rowSelected].Cells["Mahang"].Value.ToString() + "'", c);
                            _adp.Fill(dt);
                            dgv_sanpham.DataSource = dt;
                            string ngaynhap = dgv_sanpham.Rows[0].Cells[2].Value.ToString();
                            DateTime datetime = DateTime.Now;
                            txt_mahang.Text = dgv_sanpham.Rows[0].Cells[0].Value.ToString();
                            cb_tenhang.Text = dgv_sanpham.Rows[0].Cells[1].Value.ToString();
                            DateTime.TryParseExact(ngaynhap, mDatetimePattern, null, DateTimeStyles.None, out datetime);
                            dtp_ngaynhap.Value = datetime;
                            num_soluong.Value = decimal.Parse(dgv_sanpham.Rows[0].Cells[3].Value.ToString());
                            txt_ghichu.Text = dgv_sanpham.Rows[0].Cells[4].Value.ToString();
                        }
                        catch
                        {
                            cb_tenhang.Focus();
                            //txt_tenhang.SelectAll();
                        }
                    }
                }
            }
        }

        private void btn_thongke_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                dgv_sanphammoi.DataSource = null;
                dgv_sanphammoi.Rows.Clear();
                int STT_Thongke = 0;
                try
                {
                    DataTable dt = new DataTable();
                    _adp = new SQLiteDataAdapter("select * from tbl_product", c);
                    _adp.Fill(dt);
                    foreach (DataRow _Datarow in dt.Rows)
                    {
                        STT_Thongke++;
                        string[] arrItem = string.Join(",", _Datarow.ItemArray).Split(',').ToArray();
                        dgv_sanphammoi.Rows.Add(STT_Thongke, arrItem[0], arrItem[1], arrItem[2], arrItem[3], arrItem[4]);
                    }
                }
                catch (Exception ex)
                {
                    cb_tenhang.Focus();
                    //txt_tenhang.SelectAll();
                }
            }
        }

        private void AddProductForm_Load(object sender, EventArgs e)
        {
            LoadAllProductName();
        }

        private void LoadAllProductName()
        {
            cb_tenhang.Items.Clear();
            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                try
                {
                    DataTable dt = new DataTable();
                    _adp = new SQLiteDataAdapter("select * from tbl_productname", c);
                    _adp.Fill(dt);
                    foreach (DataRow _Datarow in dt.Rows)
                    {
                        string[] arrItem = string.Join(",", _Datarow.ItemArray).Split(',').ToArray();
                        cb_tenhang.Items.Add(arrItem[1]);
                    }
                }
                catch (Exception ex)
                {
                    cb_tenhang.Focus();
                }
            }
        }

        private void btn_themtenhang_Click(object sender, EventArgs e)
        {
            if (txt_tenhangmoi.Text != "")
            {
                string strInsert = string.Format("Insert into tbl_productname(name) VALUES('{0}')", txt_tenhangmoi.Text);

                using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
                {
                    c.Open();
                    try
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(strInsert, c))
                        {
                            cmd.ExecuteNonQuery();
                            cb_tenhang.Items.Add(txt_tenhangmoi.Text);
                            cb_tenhang.SelectedItem = txt_tenhangmoi.Text;
                            txt_ghichu.Focus();
                        }
                    }
                    catch (Exception ex)
                    {
                        cb_tenhang.SelectedItem = txt_tenhangmoi.Text;
                        txt_ghichu.Focus();
                    }
                }
            }
        }

        private void txt_tenhangmoi_TextChanged(object sender, EventArgs e)
        {
            TextBox t = sender as TextBox;
            if (t != null)
            {
                AutoCompleteStringCollection autoCompleteStringCollection = new AutoCompleteStringCollection();
                foreach (var name in cb_tenhang.Items)
                {
                    autoCompleteStringCollection.Add(name.ToString());
                }
                if (t.TextLength >= 2)
                {
                    t.AutoCompleteCustomSource = autoCompleteStringCollection;
                }
            }
        }

        private void btn_xoatenhang_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                string str = string.Format("DELETE FROM tbl_productname where name='{0}'", cb_tenhang.Text);
                try
                {
                    _adp.DeleteCommand = c.CreateCommand();
                    _adp.DeleteCommand.CommandText = str;
                    _adp.DeleteCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Thông báo");
                }
                LoadAllProductName();
                if (cb_tenhang.Items.Count > 0)
                    cb_tenhang.SelectedIndex = 0;
            }
        }

        private void cb_tenhang_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_ghichu.Focus();
        }
    }
}
