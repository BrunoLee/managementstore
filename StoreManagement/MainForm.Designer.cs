﻿namespace StoreManagement
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_dangbanquay = new System.Windows.Forms.Label();
            this.lbl_xinchao = new System.Windows.Forms.Label();
            this.btn_thoat = new System.Windows.Forms.Button();
            this.btn_dangxuat = new System.Windows.Forms.Button();
            this.btn_dangnhap = new System.Windows.Forms.Button();
            this.txt_matkhau = new System.Windows.Forms.TextBox();
            this.txt_taikhoan = new System.Windows.Forms.TextBox();
            this.btn_banhang = new System.Windows.Forms.Button();
            this.btn_themsanphammoi = new System.Windows.Forms.Button();
            this.lbl_login = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_matkhau = new System.Windows.Forms.Label();
            this.lbl_taikhoan = new System.Windows.Forms.Label();
            this.lbl_main3 = new System.Windows.Forms.Label();
            this.lbl_main2 = new System.Windows.Forms.Label();
            this.lbl_main1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btn_thongkegiaonhan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_dangbanquay
            // 
            this.lbl_dangbanquay.AutoSize = true;
            this.lbl_dangbanquay.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_dangbanquay.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dangbanquay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_dangbanquay.Location = new System.Drawing.Point(615, 564);
            this.lbl_dangbanquay.Name = "lbl_dangbanquay";
            this.lbl_dangbanquay.Size = new System.Drawing.Size(0, 29);
            this.lbl_dangbanquay.TabIndex = 194;
            // 
            // lbl_xinchao
            // 
            this.lbl_xinchao.AutoSize = true;
            this.lbl_xinchao.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_xinchao.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_xinchao.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_xinchao.Location = new System.Drawing.Point(613, 466);
            this.lbl_xinchao.Name = "lbl_xinchao";
            this.lbl_xinchao.Size = new System.Drawing.Size(0, 29);
            this.lbl_xinchao.TabIndex = 193;
            // 
            // btn_thoat
            // 
            this.btn_thoat.BackColor = System.Drawing.Color.Violet;
            this.btn_thoat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_thoat.ForeColor = System.Drawing.Color.Blue;
            this.btn_thoat.Image = global::StoreManagement.Properties.Resources.delete;
            this.btn_thoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_thoat.Location = new System.Drawing.Point(913, 614);
            this.btn_thoat.Name = "btn_thoat";
            this.btn_thoat.Size = new System.Drawing.Size(111, 50);
            this.btn_thoat.TabIndex = 187;
            this.btn_thoat.Text = "Thoát";
            this.btn_thoat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_thoat.UseVisualStyleBackColor = false;
            this.btn_thoat.Click += new System.EventHandler(this.btn_thoat_Click);
            // 
            // btn_dangxuat
            // 
            this.btn_dangxuat.BackColor = System.Drawing.Color.Violet;
            this.btn_dangxuat.Enabled = false;
            this.btn_dangxuat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dangxuat.ForeColor = System.Drawing.Color.Blue;
            this.btn_dangxuat.Image = global::StoreManagement.Properties.Resources.delete_home;
            this.btn_dangxuat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_dangxuat.Location = new System.Drawing.Point(741, 614);
            this.btn_dangxuat.Name = "btn_dangxuat";
            this.btn_dangxuat.Size = new System.Drawing.Size(155, 50);
            this.btn_dangxuat.TabIndex = 186;
            this.btn_dangxuat.Text = "Đăng xuất";
            this.btn_dangxuat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_dangxuat.UseVisualStyleBackColor = false;
            this.btn_dangxuat.Click += new System.EventHandler(this.btn_dangxuat_Click);
            // 
            // btn_dangnhap
            // 
            this.btn_dangnhap.BackColor = System.Drawing.Color.Violet;
            this.btn_dangnhap.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dangnhap.ForeColor = System.Drawing.Color.Blue;
            this.btn_dangnhap.Image = global::StoreManagement.Properties.Resources.add_home;
            this.btn_dangnhap.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_dangnhap.Location = new System.Drawing.Point(568, 614);
            this.btn_dangnhap.Name = "btn_dangnhap";
            this.btn_dangnhap.Size = new System.Drawing.Size(156, 50);
            this.btn_dangnhap.TabIndex = 183;
            this.btn_dangnhap.Text = "Đăng nhập";
            this.btn_dangnhap.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_dangnhap.UseVisualStyleBackColor = false;
            this.btn_dangnhap.Click += new System.EventHandler(this.btn_dangnhap_Click);
            // 
            // txt_matkhau
            // 
            this.txt_matkhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_matkhau.Location = new System.Drawing.Point(722, 501);
            this.txt_matkhau.Name = "txt_matkhau";
            this.txt_matkhau.PasswordChar = '●';
            this.txt_matkhau.Size = new System.Drawing.Size(269, 26);
            this.txt_matkhau.TabIndex = 178;
            this.txt_matkhau.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_matkhau_KeyDown);
            // 
            // txt_taikhoan
            // 
            this.txt_taikhoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_taikhoan.Location = new System.Drawing.Point(722, 453);
            this.txt_taikhoan.Name = "txt_taikhoan";
            this.txt_taikhoan.Size = new System.Drawing.Size(269, 26);
            this.txt_taikhoan.TabIndex = 173;
            this.txt_taikhoan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_taikhoan_KeyDown);
            // 
            // btn_banhang
            // 
            this.btn_banhang.BackColor = System.Drawing.Color.Violet;
            this.btn_banhang.Enabled = false;
            this.btn_banhang.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_banhang.ForeColor = System.Drawing.Color.Blue;
            this.btn_banhang.Image = global::StoreManagement.Properties.Resources.dollar_currency_sign;
            this.btn_banhang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_banhang.Location = new System.Drawing.Point(68, 510);
            this.btn_banhang.Name = "btn_banhang";
            this.btn_banhang.Size = new System.Drawing.Size(400, 50);
            this.btn_banhang.TabIndex = 188;
            this.btn_banhang.Text = "Giao nhận";
            this.btn_banhang.UseVisualStyleBackColor = false;
            this.btn_banhang.Click += new System.EventHandler(this.btn_banhang_Click);
            // 
            // btn_themsanphammoi
            // 
            this.btn_themsanphammoi.BackColor = System.Drawing.Color.Violet;
            this.btn_themsanphammoi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_themsanphammoi.Enabled = false;
            this.btn_themsanphammoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_themsanphammoi.ForeColor = System.Drawing.Color.Blue;
            this.btn_themsanphammoi.Image = global::StoreManagement.Properties.Resources.add_to_shopping_cart;
            this.btn_themsanphammoi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_themsanphammoi.Location = new System.Drawing.Point(68, 429);
            this.btn_themsanphammoi.Margin = new System.Windows.Forms.Padding(0);
            this.btn_themsanphammoi.Name = "btn_themsanphammoi";
            this.btn_themsanphammoi.Size = new System.Drawing.Size(400, 50);
            this.btn_themsanphammoi.TabIndex = 189;
            this.btn_themsanphammoi.Text = "Nhập hàng";
            this.btn_themsanphammoi.UseVisualStyleBackColor = false;
            this.btn_themsanphammoi.Click += new System.EventHandler(this.btn_themsanphammoi_Click);
            // 
            // lbl_login
            // 
            this.lbl_login.AutoSize = true;
            this.lbl_login.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_login.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_login.Location = new System.Drawing.Point(602, 388);
            this.lbl_login.Name = "lbl_login";
            this.lbl_login.Size = new System.Drawing.Size(389, 37);
            this.lbl_login.TabIndex = 180;
            this.lbl_login.Text = "ĐĂNG NHẬP HỆ THỐNG";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.ForeColor = System.Drawing.Color.Blue;
            this.lbl_name.Location = new System.Drawing.Point(613, 155);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(363, 55);
            this.lbl_name.TabIndex = 181;
            this.lbl_name.Text = "QUẢN LÝ KHO";
            this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_matkhau
            // 
            this.lbl_matkhau.AutoSize = true;
            this.lbl_matkhau.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_matkhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_matkhau.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_matkhau.Location = new System.Drawing.Point(598, 502);
            this.lbl_matkhau.Name = "lbl_matkhau";
            this.lbl_matkhau.Size = new System.Drawing.Size(91, 24);
            this.lbl_matkhau.TabIndex = 177;
            this.lbl_matkhau.Text = "Mật khẩu:";
            // 
            // lbl_taikhoan
            // 
            this.lbl_taikhoan.AutoSize = true;
            this.lbl_taikhoan.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_taikhoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_taikhoan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_taikhoan.Location = new System.Drawing.Point(598, 456);
            this.lbl_taikhoan.Name = "lbl_taikhoan";
            this.lbl_taikhoan.Size = new System.Drawing.Size(98, 24);
            this.lbl_taikhoan.TabIndex = 175;
            this.lbl_taikhoan.Text = "Tài khoản:";
            // 
            // lbl_main3
            // 
            this.lbl_main3.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_main3.ForeColor = System.Drawing.Color.Gray;
            this.lbl_main3.Location = new System.Drawing.Point(544, 369);
            this.lbl_main3.Name = "lbl_main3";
            this.lbl_main3.Size = new System.Drawing.Size(500, 322);
            this.lbl_main3.TabIndex = 184;
            // 
            // lbl_main2
            // 
            this.lbl_main2.BackColor = System.Drawing.Color.DodgerBlue;
            this.lbl_main2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lbl_main2.Location = new System.Drawing.Point(11, 369);
            this.lbl_main2.Name = "lbl_main2";
            this.lbl_main2.Size = new System.Drawing.Size(516, 322);
            this.lbl_main2.TabIndex = 185;
            // 
            // lbl_main1
            // 
            this.lbl_main1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbl_main1.ForeColor = System.Drawing.Color.Gray;
            this.lbl_main1.Location = new System.Drawing.Point(544, 10);
            this.lbl_main1.Name = "lbl_main1";
            this.lbl_main1.Size = new System.Drawing.Size(500, 345);
            this.lbl_main1.TabIndex = 182;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StoreManagement.Properties.Resources.main_image;
            this.pictureBox1.Location = new System.Drawing.Point(11, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(516, 345);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 172;
            this.pictureBox1.TabStop = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Violet;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.Blue;
            this.btnAdd.Image = global::StoreManagement.Properties.Resources.add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(913, 550);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(111, 50);
            this.btnAdd.TabIndex = 195;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btn_thongkegiaonhan
            // 
            this.btn_thongkegiaonhan.BackColor = System.Drawing.Color.Violet;
            this.btn_thongkegiaonhan.Enabled = false;
            this.btn_thongkegiaonhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_thongkegiaonhan.ForeColor = System.Drawing.Color.Blue;
            this.btn_thongkegiaonhan.Image = global::StoreManagement.Properties.Resources.calculator;
            this.btn_thongkegiaonhan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_thongkegiaonhan.Location = new System.Drawing.Point(68, 591);
            this.btn_thongkegiaonhan.Name = "btn_thongkegiaonhan";
            this.btn_thongkegiaonhan.Size = new System.Drawing.Size(400, 49);
            this.btn_thongkegiaonhan.TabIndex = 197;
            this.btn_thongkegiaonhan.Text = "Thống kê giao nhận";
            this.btn_thongkegiaonhan.UseVisualStyleBackColor = false;
            this.btn_thongkegiaonhan.Click += new System.EventHandler(this.btn_thongkegiaonhan_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1055, 700);
            this.Controls.Add(this.btn_thongkegiaonhan);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbl_dangbanquay);
            this.Controls.Add(this.lbl_xinchao);
            this.Controls.Add(this.btn_thoat);
            this.Controls.Add(this.btn_dangxuat);
            this.Controls.Add(this.btn_dangnhap);
            this.Controls.Add(this.txt_matkhau);
            this.Controls.Add(this.txt_taikhoan);
            this.Controls.Add(this.btn_banhang);
            this.Controls.Add(this.btn_themsanphammoi);
            this.Controls.Add(this.lbl_login);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.lbl_matkhau);
            this.Controls.Add(this.lbl_taikhoan);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbl_main3);
            this.Controls.Add(this.lbl_main2);
            this.Controls.Add(this.lbl_main1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản Lý Kho Hàng";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_dangbanquay;
        private System.Windows.Forms.Label lbl_xinchao;
        private System.Windows.Forms.Button btn_thoat;
        private System.Windows.Forms.Button btn_dangxuat;
        private System.Windows.Forms.Button btn_dangnhap;
        private System.Windows.Forms.TextBox txt_matkhau;
        private System.Windows.Forms.TextBox txt_taikhoan;
        private System.Windows.Forms.Button btn_banhang;
        private System.Windows.Forms.Button btn_themsanphammoi;
        private System.Windows.Forms.Label lbl_login;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_matkhau;
        private System.Windows.Forms.Label lbl_taikhoan;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_main3;
        private System.Windows.Forms.Label lbl_main2;
        private System.Windows.Forms.Label lbl_main1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btn_thongkegiaonhan;
    }
}