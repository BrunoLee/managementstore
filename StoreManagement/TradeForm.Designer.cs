﻿namespace StoreManagement
{
    partial class TradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.productiondataBindingSource7 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dgv_sanphammoi = new System.Windows.Forms.DataGridView();
            this.lbl_danhsach = new System.Windows.Forms.Label();
            this.dgv_sanpham = new System.Windows.Forms.DataGridView();
            this.sqLiteCommand1 = new System.Data.SQLite.SQLiteCommand();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.num_tradeQtt = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_backmain = new System.Windows.Forms.Button();
            this.lbl_quanlity = new System.Windows.Forms.Label();
            this.txt_ghichu = new System.Windows.Forms.TextBox();
            this.lbl_productname = new System.Windows.Forms.Label();
            this.txt_mahang = new System.Windows.Forms.TextBox();
            this.lbl_label = new System.Windows.Forms.Label();
            this.txt_tenhang = new System.Windows.Forms.TextBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.num_storeQtt = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.STT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ngay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mahang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tenhang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Soluongkho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Soluong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ghichu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_finish = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanphammoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanpham)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_tradeQtt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_storeQtt)).BeginInit();
            this.SuspendLayout();
            // 
            // productiondataBindingSource7
            // 
            this.productiondataBindingSource7.DataMember = "production_data";
            // 
            // productiondataBindingSource
            // 
            this.productiondataBindingSource.DataMember = "production_data";
            // 
            // productiondataBindingSource4
            // 
            this.productiondataBindingSource4.DataMember = "production_data";
            // 
            // productiondataBindingSource5
            // 
            this.productiondataBindingSource5.DataMember = "production_data";
            // 
            // productiondataBindingSource6
            // 
            this.productiondataBindingSource6.DataMember = "production_data";
            // 
            // productiondataBindingSource2
            // 
            this.productiondataBindingSource2.DataMember = "production_data";
            // 
            // productiondataBindingSource3
            // 
            this.productiondataBindingSource3.DataMember = "production_data";
            // 
            // productiondataBindingSource1
            // 
            this.productiondataBindingSource1.DataMember = "production_data";
            // 
            // dgv_sanphammoi
            // 
            this.dgv_sanphammoi.AllowUserToAddRows = false;
            this.dgv_sanphammoi.AllowUserToDeleteRows = false;
            this.dgv_sanphammoi.AllowUserToResizeColumns = false;
            this.dgv_sanphammoi.AllowUserToResizeRows = false;
            this.dgv_sanphammoi.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_sanphammoi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_sanphammoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_sanphammoi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT,
            this.Ngay,
            this.Mahang,
            this.Tenhang,
            this.Soluongkho,
            this.Soluong,
            this.Ghichu});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanphammoi.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_sanphammoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_sanphammoi.Location = new System.Drawing.Point(0, 0);
            this.dgv_sanphammoi.MultiSelect = false;
            this.dgv_sanphammoi.Name = "dgv_sanphammoi";
            this.dgv_sanphammoi.ReadOnly = true;
            this.dgv_sanphammoi.RowHeadersVisible = false;
            this.dgv_sanphammoi.RowHeadersWidth = 50;
            this.dgv_sanphammoi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_sanphammoi.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanphammoi.Size = new System.Drawing.Size(1332, 316);
            this.dgv_sanphammoi.TabIndex = 111;
            this.dgv_sanphammoi.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_sanphammoi_CellMouseClick);
            // 
            // lbl_danhsach
            // 
            this.lbl_danhsach.BackColor = System.Drawing.Color.Azure;
            this.lbl_danhsach.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_danhsach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_danhsach.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_danhsach.ForeColor = System.Drawing.Color.Blue;
            this.lbl_danhsach.Location = new System.Drawing.Point(0, 400);
            this.lbl_danhsach.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_danhsach.Name = "lbl_danhsach";
            this.lbl_danhsach.Size = new System.Drawing.Size(1332, 40);
            this.lbl_danhsach.TabIndex = 105;
            this.lbl_danhsach.Text = "DANH SÁCH GIAO NHẬN";
            this.lbl_danhsach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgv_sanpham
            // 
            this.dgv_sanpham.AllowUserToAddRows = false;
            this.dgv_sanpham.AllowUserToDeleteRows = false;
            this.dgv_sanpham.AllowUserToResizeColumns = false;
            this.dgv_sanpham.AllowUserToResizeRows = false;
            this.dgv_sanpham.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_sanpham.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_sanpham.Location = new System.Drawing.Point(241, 111);
            this.dgv_sanpham.MultiSelect = false;
            this.dgv_sanpham.Name = "dgv_sanpham";
            this.dgv_sanpham.ReadOnly = true;
            this.dgv_sanpham.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_sanpham.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanpham.Size = new System.Drawing.Size(574, 128);
            this.dgv_sanpham.TabIndex = 113;
            // 
            // sqLiteCommand1
            // 
            this.sqLiteCommand1.CommandText = null;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_danhsach, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1332, 756);
            this.tableLayoutPanel1.TabIndex = 119;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_sanphammoi);
            this.panel2.Controls.Add(this.dgv_sanpham);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 440);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1332, 316);
            this.panel2.TabIndex = 106;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.PaleGreen;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1000F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1332, 400);
            this.tableLayoutPanel2.TabIndex = 107;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleGreen;
            this.panel1.Controls.Add(this.btn_finish);
            this.panel1.Controls.Add(this.num_storeQtt);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btn_delete);
            this.panel1.Controls.Add(this.txt_tenhang);
            this.panel1.Controls.Add(this.num_tradeQtt);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.btn_backmain);
            this.panel1.Controls.Add(this.lbl_quanlity);
            this.panel1.Controls.Add(this.txt_ghichu);
            this.panel1.Controls.Add(this.lbl_productname);
            this.panel1.Controls.Add(this.txt_mahang);
            this.panel1.Controls.Add(this.lbl_label);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(166, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 400);
            this.panel1.TabIndex = 2;
            // 
            // num_tradeQtt
            // 
            this.num_tradeQtt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_tradeQtt.Location = new System.Drawing.Point(190, 146);
            this.num_tradeQtt.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.num_tradeQtt.Name = "num_tradeQtt";
            this.num_tradeQtt.Size = new System.Drawing.Size(83, 26);
            this.num_tradeQtt.TabIndex = 115;
            this.num_tradeQtt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.PaleGreen;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(82, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 24);
            this.label1.TabIndex = 114;
            this.label1.Text = "Số lượng:";
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.Violet;
            this.btn_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.Blue;
            this.btn_Save.Image = global::StoreManagement.Properties.Resources.add_page;
            this.btn_Save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Save.Location = new System.Drawing.Point(175, 321);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(150, 49);
            this.btn_Save.TabIndex = 108;
            this.btn_Save.Text = "Nhập";
            this.btn_Save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Save.UseVisualStyleBackColor = false;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_backmain
            // 
            this.btn_backmain.BackColor = System.Drawing.Color.Violet;
            this.btn_backmain.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_backmain.ForeColor = System.Drawing.Color.Blue;
            this.btn_backmain.Image = global::StoreManagement.Properties.Resources.back;
            this.btn_backmain.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_backmain.Location = new System.Drawing.Point(700, 322);
            this.btn_backmain.Name = "btn_backmain";
            this.btn_backmain.Size = new System.Drawing.Size(150, 50);
            this.btn_backmain.TabIndex = 110;
            this.btn_backmain.Text = "Trở về";
            this.btn_backmain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_backmain.UseVisualStyleBackColor = false;
            this.btn_backmain.Click += new System.EventHandler(this.btn_backmain_Click);
            // 
            // lbl_quanlity
            // 
            this.lbl_quanlity.AutoSize = true;
            this.lbl_quanlity.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_quanlity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_quanlity.ForeColor = System.Drawing.Color.Blue;
            this.lbl_quanlity.Location = new System.Drawing.Point(92, 192);
            this.lbl_quanlity.Name = "lbl_quanlity";
            this.lbl_quanlity.Size = new System.Drawing.Size(81, 24);
            this.lbl_quanlity.TabIndex = 104;
            this.lbl_quanlity.Text = "Ghi chú:";
            // 
            // txt_ghichu
            // 
            this.txt_ghichu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ghichu.Location = new System.Drawing.Point(190, 190);
            this.txt_ghichu.Multiline = true;
            this.txt_ghichu.Name = "txt_ghichu";
            this.txt_ghichu.Size = new System.Drawing.Size(645, 53);
            this.txt_ghichu.TabIndex = 107;
            // 
            // lbl_productname
            // 
            this.lbl_productname.AutoSize = true;
            this.lbl_productname.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_productname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_productname.ForeColor = System.Drawing.Color.Blue;
            this.lbl_productname.Location = new System.Drawing.Point(76, 98);
            this.lbl_productname.Name = "lbl_productname";
            this.lbl_productname.Size = new System.Drawing.Size(97, 24);
            this.lbl_productname.TabIndex = 103;
            this.lbl_productname.Text = "Tên hàng:";
            // 
            // txt_mahang
            // 
            this.txt_mahang.BackColor = System.Drawing.SystemColors.Window;
            this.txt_mahang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mahang.Location = new System.Drawing.Point(190, 52);
            this.txt_mahang.MaxLength = 40;
            this.txt_mahang.Name = "txt_mahang";
            this.txt_mahang.Size = new System.Drawing.Size(644, 26);
            this.txt_mahang.TabIndex = 101;
            this.txt_mahang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_mahang_KeyDown);
            // 
            // lbl_label
            // 
            this.lbl_label.AutoSize = true;
            this.lbl_label.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_label.ForeColor = System.Drawing.Color.Blue;
            this.lbl_label.Location = new System.Drawing.Point(84, 52);
            this.lbl_label.Name = "lbl_label";
            this.lbl_label.Size = new System.Drawing.Size(89, 24);
            this.lbl_label.TabIndex = 106;
            this.lbl_label.Text = "Mã hàng:";
            // 
            // txt_tenhang
            // 
            this.txt_tenhang.BackColor = System.Drawing.SystemColors.Window;
            this.txt_tenhang.Enabled = false;
            this.txt_tenhang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tenhang.Location = new System.Drawing.Point(191, 99);
            this.txt_tenhang.MaxLength = 40;
            this.txt_tenhang.Name = "txt_tenhang";
            this.txt_tenhang.Size = new System.Drawing.Size(644, 26);
            this.txt_tenhang.TabIndex = 119;
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Violet;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.ForeColor = System.Drawing.Color.Blue;
            this.btn_delete.Image = global::StoreManagement.Properties.Resources.delete_page;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(350, 322);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(150, 50);
            this.btn_delete.TabIndex = 120;
            this.btn_delete.Text = "Xóa";
            this.btn_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // num_storeQtt
            // 
            this.num_storeQtt.Enabled = false;
            this.num_storeQtt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_storeQtt.Location = new System.Drawing.Point(492, 144);
            this.num_storeQtt.Name = "num_storeQtt";
            this.num_storeQtt.Size = new System.Drawing.Size(83, 26);
            this.num_storeQtt.TabIndex = 122;
            this.num_storeQtt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.PaleGreen;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(339, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 24);
            this.label2.TabIndex = 121;
            this.label2.Text = "Số lượng kho:";
            // 
            // STT
            // 
            this.STT.HeaderText = "STT";
            this.STT.Name = "STT";
            this.STT.ReadOnly = true;
            this.STT.Width = 50;
            // 
            // Ngay
            // 
            this.Ngay.HeaderText = "Ngày";
            this.Ngay.Name = "Ngay";
            this.Ngay.ReadOnly = true;
            this.Ngay.Width = 200;
            // 
            // Mahang
            // 
            this.Mahang.HeaderText = "Mã hàng";
            this.Mahang.Name = "Mahang";
            this.Mahang.ReadOnly = true;
            this.Mahang.Width = 250;
            // 
            // Tenhang
            // 
            this.Tenhang.HeaderText = "Tên hàng";
            this.Tenhang.Name = "Tenhang";
            this.Tenhang.ReadOnly = true;
            this.Tenhang.Width = 200;
            // 
            // Soluongkho
            // 
            this.Soluongkho.HeaderText = "Số lượng kho";
            this.Soluongkho.Name = "Soluongkho";
            this.Soluongkho.ReadOnly = true;
            // 
            // Soluong
            // 
            this.Soluong.HeaderText = "Số lượng giao/nhận";
            this.Soluong.Name = "Soluong";
            this.Soluong.ReadOnly = true;
            // 
            // Ghichu
            // 
            this.Ghichu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Ghichu.HeaderText = "Ghi chú";
            this.Ghichu.Name = "Ghichu";
            this.Ghichu.ReadOnly = true;
            // 
            // btn_finish
            // 
            this.btn_finish.BackColor = System.Drawing.Color.Violet;
            this.btn_finish.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_finish.ForeColor = System.Drawing.Color.Blue;
            this.btn_finish.Image = global::StoreManagement.Properties.Resources.edit;
            this.btn_finish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_finish.Location = new System.Drawing.Point(525, 322);
            this.btn_finish.Name = "btn_finish";
            this.btn_finish.Size = new System.Drawing.Size(150, 50);
            this.btn_finish.TabIndex = 123;
            this.btn_finish.Text = "Kết thúc";
            this.btn_finish.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_finish.UseVisualStyleBackColor = false;
            this.btn_finish.Click += new System.EventHandler(this.btn_finish_Click);
            // 
            // TradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1332, 756);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TradeForm";
            this.Text = "TradeForm";
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanphammoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanpham)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_tradeQtt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_storeQtt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource productiondataBindingSource7;
        private System.Windows.Forms.BindingSource productiondataBindingSource;
        private System.Windows.Forms.BindingSource productiondataBindingSource4;
        private System.Windows.Forms.BindingSource productiondataBindingSource5;
        private System.Windows.Forms.BindingSource productiondataBindingSource6;
        private System.Windows.Forms.BindingSource productiondataBindingSource2;
        private System.Windows.Forms.BindingSource productiondataBindingSource3;
        private System.Windows.Forms.BindingSource productiondataBindingSource1;
        private System.Windows.Forms.DataGridView dgv_sanphammoi;
        private System.Windows.Forms.Label lbl_danhsach;
        private System.Windows.Forms.DataGridView dgv_sanpham;
        private System.Data.SQLite.SQLiteCommand sqLiteCommand1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown num_tradeQtt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_backmain;
        private System.Windows.Forms.Label lbl_quanlity;
        private System.Windows.Forms.TextBox txt_ghichu;
        private System.Windows.Forms.Label lbl_productname;
        private System.Windows.Forms.TextBox txt_mahang;
        private System.Windows.Forms.Label lbl_label;
        private System.Windows.Forms.TextBox txt_tenhang;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.NumericUpDown num_storeQtt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ngay;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mahang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tenhang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Soluongkho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Soluong;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ghichu;
        private System.Windows.Forms.Button btn_finish;
    }
}