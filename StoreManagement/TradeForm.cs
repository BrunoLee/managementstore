﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement
{
    public partial class TradeForm : Form
    {
        MainForm mMainForm;
        string mUser = "";
        int rowSelected = 0;
        SQLiteDataAdapter _adp = new SQLiteDataAdapter();
        string mDatetimePattern = "dd/MM/yyyy HH:mm:ss";
        string mConnectionString = "Data Source = MyDatabase.sqlite;Version=3;";
        string mStoreQuantity = "";
        public TradeForm(MainForm mainForm, string user)
        {
            InitializeComponent();
            mMainForm = mainForm;
            mUser = user;
            this.WindowState = FormWindowState.Maximized;
        }

        private void btn_backmain_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn sẵn sàng xóa hết các thông tin và rời khỏi?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK) ;
            {
                this.Dispose();
                this.Close();
                mMainForm.Show();
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            AddToTable();
            //SaveData();
        }

        private void AddToTable()
        {
            if (num_storeQtt.Value < num_tradeQtt.Value)
            {
                MessageBox.Show("Số lượng hàng trong kho không đủ!", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                num_tradeQtt.Focus();
            }
            else
            {
                string ngaynhap = DateTime.Now.ToString(mDatetimePattern);
                if (!isUpdate)
                {
                    this.dgv_sanphammoi.Rows.Add(dgv_sanphammoi.Rows.Count, ngaynhap, txt_mahang.Text, txt_tenhang.Text, num_storeQtt.Value.ToString(), num_tradeQtt.Value.ToString(), txt_ghichu.Text);
                }
                else
                {
                    foreach (DataGridViewRow row in dgv_sanphammoi.Rows)
                    {
                        if (row.Cells[1].Value.ToString() == txt_mahang.Text)
                        {
                            rowSelected = row.Index;
                        }
                    }
                    if (rowSelected > -1 && dgv_sanphammoi.Rows.Count > 0)
                    {
                        dgv_sanphammoi.Rows.RemoveAt(rowSelected);
                    }
                    this.dgv_sanphammoi.Rows.Insert(rowSelected, rowSelected, ngaynhap, txt_mahang.Text, txt_tenhang.Text, num_storeQtt.Value.ToString(), num_tradeQtt.Value.ToString(), txt_ghichu.Text);
                    isUpdate = false;
                }
            }
        }
        private void SaveData(int rowIdx)
        {
            string ngaynhap = dgv_sanphammoi.Rows[rowIdx].Cells["Ngay"].Value.ToString();
            string mahang = dgv_sanphammoi.Rows[rowIdx].Cells["Mahang"].Value.ToString();
            string tenhang = dgv_sanphammoi.Rows[rowIdx].Cells["Tenhang"].Value.ToString();
            string user = mUser;
            string storeqtt = dgv_sanphammoi.Rows[rowIdx].Cells["Soluongkho"].Value.ToString();
            string tradeqtt = dgv_sanphammoi.Rows[rowIdx].Cells["Soluong"].Value.ToString();
            string note = dgv_sanphammoi.Rows[rowIdx].Cells["Ghichu"].Value.ToString();

            int tradetype = 0;
            if (int.Parse(tradeqtt) < 0)
            {
                tradetype = -1;
            }
            else
                tradetype = 1;

            string strInsert = string.Format("Insert into tbl_lending(datetime,serial,name,person,storeqtt,tradeqtt,tradetype,note) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", ngaynhap, mahang, tenhang, user, storeqtt, tradeqtt, tradetype, note);

            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(strInsert, c))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (rowSelected > -1 && dgv_sanphammoi.Rows.Count > 0)
            {
                dgv_sanphammoi.Rows.RemoveAt(rowSelected);
            }
        }

        bool isUpdate = false;
        private void dgv_sanphammoi_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                rowSelected = e.RowIndex;
                if (e.RowIndex != -1 && dgv_sanphammoi.Rows[rowSelected].Cells["Mahang"].Value != null)
                {
                    txt_tenhang.Text = dgv_sanphammoi.Rows[rowSelected].Cells["Tenhang"].Value.ToString();
                    txt_mahang.Text = dgv_sanphammoi.Rows[rowSelected].Cells["Mahang"].Value.ToString();
                    num_tradeQtt.Value = decimal.Parse(dgv_sanphammoi.Rows[rowSelected].Cells["Soluong"].Value.ToString());
                    num_storeQtt.Value = decimal.Parse(dgv_sanphammoi.Rows[rowSelected].Cells["Soluongkho"].Value.ToString());
                    txt_ghichu.Text = dgv_sanphammoi.Rows[rowSelected].Cells["Ghichu"].Value.ToString();
                    isUpdate = true;
                }
            }
        }

        private void txt_mahang_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
                {
                    c.Open();
                    dgv_sanpham.DataSource = null;
                    dgv_sanpham.Rows.Clear();
                    try
                    {
                        DataTable dt = new DataTable();
                        _adp = new SQLiteDataAdapter("select * from tbl_product where serial =" + "'" + txt_mahang.Text + "'", c);
                        _adp.Fill(dt);
                        dgv_sanpham.DataSource = dt;
                        txt_tenhang.Text = dgv_sanpham.Rows[0].Cells[1].Value.ToString();
                        mStoreQuantity = dgv_sanpham.Rows[0].Cells[3].Value.ToString();
                        decimal soluong = 0;
                        decimal.TryParse(mStoreQuantity, out soluong);
                        num_storeQtt.Value = soluong;

                        txt_ghichu.Clear();
                        txt_ghichu.Focus();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Không có mã hàng trên hệ thống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txt_mahang.SelectAll();
                        txt_mahang.Focus();
                    }
                }
            }
        }

        private void btn_finish_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tiếp tục lưu các thông tin và rời khỏi?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK) ;
            {
                for (int i = 0; i < dgv_sanphammoi.Rows.Count; i++)
                {
                    SaveData(i);
                }

                this.Dispose();
                this.Close();
                mMainForm.Show();
            }
            
        }
    }
}
