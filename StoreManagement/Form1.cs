﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement
{
    public partial class Form1 : Form
    {
        SQLiteConnection _con = new SQLiteConnection();
        public Form1()
        {
            InitializeComponent();
            createTable();
        }

       
        public void createConection()
        {
            string _strConnect = "Data Source=MyDatabase.sqlite;Version=3;";
            _con.ConnectionString = _strConnect;
            _con.Open();
        }

        public void closeConnection()
        {
            _con.Close();
        }

        public void createTable()
        {
            string sql = "CREATE TABLE IF NOT EXISTS tbl_product ([id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, serial nvarchar(50), name varchar(15), receiveddate varchar(30),quantity INTEGER, note nvarchar(200))";
            SQLiteConnection.CreateFile("MyDatabase.sqlite");
            createConection();
            SQLiteCommand command = new SQLiteCommand(sql, _con);
            command.ExecuteNonQuery();
            closeConnection();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //createTable();
            loadDataToGrid();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtID.Clear();
            txtFullname.Clear();
            txtBirthday.Clear();
            txtAddress.Clear();
            txtFullname.Focus();
        }
        public DataSet loadData()
        {
            DataSet ds = new DataSet();
            createConection();
            SQLiteDataAdapter da = new SQLiteDataAdapter("select id, serial as [Serial Number], name as [Name], receivedate as [Received Date], lenddate as [Lend Date], note as [Note], available as [Available] from tbl_product", _con);

            da.Fill(ds);
            closeConnection();
            return ds;
        }

        public void loadDataToGrid()
        {
            DataSet ds = loadData();
            gvDataStore.DataSource = ds.Tables[0];
            txtID.DataBindings.Clear();
            txtFullname.DataBindings.Clear();
            txtBirthday.DataBindings.Clear();
            txtAddress.DataBindings.Clear();
            txtID.DataBindings.Add("text", ds.Tables[0], "id");
            txtFullname.DataBindings.Add("text", ds.Tables[0], "Full name");
            txtBirthday.DataBindings.Add("text", ds.Tables[0], "Birthday");
            txtAddress.DataBindings.Add("text", ds.Tables[0], "Address");
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            string fullname = txtFullname.Text;
            string birthday = txtBirthday.Text;
            string address = txtAddress.Text;

            string strInsert = string.Format("INSERT INTO tbl_students(fullname, birthday, email, address, phone) VALUES('{0}','{1}','{2}','{3}','{4}')", fullname, birthday, address);

            createConection();
            SQLiteCommand cmd = new SQLiteCommand(strInsert, _con);
            cmd.ExecuteNonQuery();
            closeConnection();

            // load data
            loadDataToGrid();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string id = txtID.Text;
            string fullname = txtFullname.Text;
            string birthday = txtBirthday.Text;
            string address = txtAddress.Text;

            string strInsert = string.Format("UPDATE tbl_students set fullname='{0}', birthday='{1}', email='{2}', address='{3}', phone='{4}' where id='{5}'", fullname, birthday, address, id);

            createConection();
            SQLiteCommand cmd = new SQLiteCommand(strInsert, _con);
            cmd.ExecuteNonQuery();
            closeConnection();

            // load data
            loadDataToGrid();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string id = txtID.Text;

            string strInsert = string.Format("DELETE FROM tbl_students where id='{0}'", id);

            createConection();
            SQLiteCommand cmd = new SQLiteCommand(strInsert, _con);
            cmd.ExecuteNonQuery();
            closeConnection();

            // load data
            loadDataToGrid();
        }
    }
}
