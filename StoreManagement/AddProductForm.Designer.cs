﻿namespace StoreManagement
{
    partial class AddProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_backmain = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.dgv_sanphammoi = new System.Windows.Forms.DataGridView();
            this.STT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mahang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tenhang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ngaynhap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Giaban = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nhomhang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_quanlity = new System.Windows.Forms.Label();
            this.lbl_productname = new System.Windows.Forms.Label();
            this.lbl_danhsach = new System.Windows.Forms.Label();
            this.lbl_label = new System.Windows.Forms.Label();
            this.txt_ghichu = new System.Windows.Forms.TextBox();
            this.txt_mahang = new System.Windows.Forms.TextBox();
            this.dgv_sanpham = new System.Windows.Forms.DataGridView();
            this.productiondataBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productiondataBindingSource7 = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.num_soluong = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_xoatenhang = new System.Windows.Forms.Button();
            this.btn_themtenhang = new System.Windows.Forms.Button();
            this.lbl_tenhangmoi = new System.Windows.Forms.Label();
            this.txt_tenhangmoi = new System.Windows.Forms.TextBox();
            this.btn_thongke = new System.Windows.Forms.Button();
            this.cb_tenhang = new System.Windows.Forms.ComboBox();
            this.dtp_ngaynhap = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanphammoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanpham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_soluong)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_backmain
            // 
            this.btn_backmain.BackColor = System.Drawing.Color.Violet;
            this.btn_backmain.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_backmain.ForeColor = System.Drawing.Color.Blue;
            this.btn_backmain.Image = global::StoreManagement.Properties.Resources.back;
            this.btn_backmain.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_backmain.Location = new System.Drawing.Point(681, 366);
            this.btn_backmain.Name = "btn_backmain";
            this.btn_backmain.Size = new System.Drawing.Size(150, 50);
            this.btn_backmain.TabIndex = 92;
            this.btn_backmain.Text = "Trở về";
            this.btn_backmain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_backmain.UseVisualStyleBackColor = false;
            this.btn_backmain.Click += new System.EventHandler(this.btn_backmain_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Violet;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.ForeColor = System.Drawing.Color.Blue;
            this.btn_delete.Image = global::StoreManagement.Properties.Resources.delete_page;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(509, 366);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(150, 50);
            this.btn_delete.TabIndex = 91;
            this.btn_delete.Text = "Xóa";
            this.btn_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.BackColor = System.Drawing.Color.Violet;
            this.btn_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.ForeColor = System.Drawing.Color.Blue;
            this.btn_Save.Image = global::StoreManagement.Properties.Resources.add_page;
            this.btn_Save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Save.Location = new System.Drawing.Point(337, 366);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(150, 50);
            this.btn_Save.TabIndex = 90;
            this.btn_Save.Text = "Lưu";
            this.btn_Save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Save.UseVisualStyleBackColor = false;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // dgv_sanphammoi
            // 
            this.dgv_sanphammoi.AllowUserToAddRows = false;
            this.dgv_sanphammoi.AllowUserToDeleteRows = false;
            this.dgv_sanphammoi.AllowUserToResizeColumns = false;
            this.dgv_sanphammoi.AllowUserToResizeRows = false;
            this.dgv_sanphammoi.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_sanphammoi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgv_sanphammoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_sanphammoi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT,
            this.Mahang,
            this.Tenhang,
            this.Ngaynhap,
            this.Giaban,
            this.Nhomhang});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanphammoi.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgv_sanphammoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_sanphammoi.Location = new System.Drawing.Point(0, 0);
            this.dgv_sanphammoi.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_sanphammoi.MultiSelect = false;
            this.dgv_sanphammoi.Name = "dgv_sanphammoi";
            this.dgv_sanphammoi.ReadOnly = true;
            this.dgv_sanphammoi.RowHeadersVisible = false;
            this.dgv_sanphammoi.RowHeadersWidth = 50;
            this.dgv_sanphammoi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_sanphammoi.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanphammoi.Size = new System.Drawing.Size(1185, 159);
            this.dgv_sanphammoi.TabIndex = 93;
            this.dgv_sanphammoi.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_sanphammoi_CellMouseClick);
            // 
            // STT
            // 
            this.STT.HeaderText = "STT";
            this.STT.Name = "STT";
            this.STT.ReadOnly = true;
            this.STT.Width = 50;
            // 
            // Mahang
            // 
            this.Mahang.HeaderText = "Mã hàng";
            this.Mahang.Name = "Mahang";
            this.Mahang.ReadOnly = true;
            this.Mahang.Width = 250;
            // 
            // Tenhang
            // 
            this.Tenhang.HeaderText = "Tên hàng";
            this.Tenhang.Name = "Tenhang";
            this.Tenhang.ReadOnly = true;
            this.Tenhang.Width = 300;
            // 
            // Ngaynhap
            // 
            this.Ngaynhap.HeaderText = "Ngày Nhập";
            this.Ngaynhap.Name = "Ngaynhap";
            this.Ngaynhap.ReadOnly = true;
            this.Ngaynhap.Width = 150;
            // 
            // Giaban
            // 
            this.Giaban.HeaderText = "Số lượng";
            this.Giaban.Name = "Giaban";
            this.Giaban.ReadOnly = true;
            // 
            // Nhomhang
            // 
            this.Nhomhang.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nhomhang.HeaderText = "Ghi chú";
            this.Nhomhang.Name = "Nhomhang";
            this.Nhomhang.ReadOnly = true;
            // 
            // lbl_quanlity
            // 
            this.lbl_quanlity.AutoSize = true;
            this.lbl_quanlity.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_quanlity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_quanlity.ForeColor = System.Drawing.Color.Blue;
            this.lbl_quanlity.Location = new System.Drawing.Point(67, 284);
            this.lbl_quanlity.Name = "lbl_quanlity";
            this.lbl_quanlity.Size = new System.Drawing.Size(81, 24);
            this.lbl_quanlity.TabIndex = 72;
            this.lbl_quanlity.Text = "Ghi chú:";
            // 
            // lbl_productname
            // 
            this.lbl_productname.AutoSize = true;
            this.lbl_productname.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_productname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_productname.ForeColor = System.Drawing.Color.Blue;
            this.lbl_productname.Location = new System.Drawing.Point(51, 96);
            this.lbl_productname.Name = "lbl_productname";
            this.lbl_productname.Size = new System.Drawing.Size(97, 24);
            this.lbl_productname.TabIndex = 71;
            this.lbl_productname.Text = "Tên hàng:";
            // 
            // lbl_danhsach
            // 
            this.lbl_danhsach.BackColor = System.Drawing.Color.Azure;
            this.lbl_danhsach.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_danhsach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_danhsach.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_danhsach.ForeColor = System.Drawing.Color.Blue;
            this.lbl_danhsach.Location = new System.Drawing.Point(0, 450);
            this.lbl_danhsach.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_danhsach.Name = "lbl_danhsach";
            this.lbl_danhsach.Size = new System.Drawing.Size(1185, 40);
            this.lbl_danhsach.TabIndex = 73;
            this.lbl_danhsach.Text = "DANH SÁCH HÀNG MỚI";
            this.lbl_danhsach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_label
            // 
            this.lbl_label.AutoSize = true;
            this.lbl_label.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_label.ForeColor = System.Drawing.Color.Blue;
            this.lbl_label.Location = new System.Drawing.Point(59, 42);
            this.lbl_label.Name = "lbl_label";
            this.lbl_label.Size = new System.Drawing.Size(89, 24);
            this.lbl_label.TabIndex = 74;
            this.lbl_label.Text = "Mã hàng:";
            // 
            // txt_ghichu
            // 
            this.txt_ghichu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ghichu.Location = new System.Drawing.Point(165, 288);
            this.txt_ghichu.Multiline = true;
            this.txt_ghichu.Name = "txt_ghichu";
            this.txt_ghichu.Size = new System.Drawing.Size(668, 53);
            this.txt_ghichu.TabIndex = 75;
            // 
            // txt_mahang
            // 
            this.txt_mahang.BackColor = System.Drawing.SystemColors.Window;
            this.txt_mahang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_mahang.Location = new System.Drawing.Point(165, 44);
            this.txt_mahang.MaxLength = 40;
            this.txt_mahang.Name = "txt_mahang";
            this.txt_mahang.Size = new System.Drawing.Size(668, 26);
            this.txt_mahang.TabIndex = 64;
            this.txt_mahang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_mahang_KeyDown);
            // 
            // dgv_sanpham
            // 
            this.dgv_sanpham.AllowUserToAddRows = false;
            this.dgv_sanpham.AllowUserToDeleteRows = false;
            this.dgv_sanpham.AllowUserToResizeColumns = false;
            this.dgv_sanpham.AllowUserToResizeRows = false;
            this.dgv_sanpham.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_sanpham.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_sanpham.Location = new System.Drawing.Point(139, 53);
            this.dgv_sanpham.MultiSelect = false;
            this.dgv_sanpham.Name = "dgv_sanpham";
            this.dgv_sanpham.ReadOnly = true;
            this.dgv_sanpham.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_sanpham.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_sanpham.Size = new System.Drawing.Size(574, 128);
            this.dgv_sanpham.TabIndex = 97;
            // 
            // productiondataBindingSource1
            // 
            this.productiondataBindingSource1.DataMember = "production_data";
            // 
            // productiondataBindingSource3
            // 
            this.productiondataBindingSource3.DataMember = "production_data";
            // 
            // productiondataBindingSource2
            // 
            this.productiondataBindingSource2.DataMember = "production_data";
            // 
            // productiondataBindingSource6
            // 
            this.productiondataBindingSource6.DataMember = "production_data";
            // 
            // productiondataBindingSource5
            // 
            this.productiondataBindingSource5.DataMember = "production_data";
            // 
            // productiondataBindingSource4
            // 
            this.productiondataBindingSource4.DataMember = "production_data";
            // 
            // productiondataBindingSource
            // 
            this.productiondataBindingSource.DataMember = "production_data";
            // 
            // productiondataBindingSource7
            // 
            this.productiondataBindingSource7.DataMember = "production_data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.PaleGreen;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(57, 243);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 24);
            this.label1.TabIndex = 99;
            this.label1.Text = "Số lượng:";
            // 
            // num_soluong
            // 
            this.num_soluong.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_soluong.Location = new System.Drawing.Point(165, 245);
            this.num_soluong.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_soluong.Name = "num_soluong";
            this.num_soluong.Size = new System.Drawing.Size(69, 26);
            this.num_soluong.TabIndex = 100;
            this.num_soluong.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_danhsach, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1185, 649);
            this.tableLayoutPanel1.TabIndex = 101;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.PaleGreen;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1000F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1185, 450);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleGreen;
            this.panel1.Controls.Add(this.btn_xoatenhang);
            this.panel1.Controls.Add(this.btn_themtenhang);
            this.panel1.Controls.Add(this.lbl_tenhangmoi);
            this.panel1.Controls.Add(this.txt_tenhangmoi);
            this.panel1.Controls.Add(this.btn_thongke);
            this.panel1.Controls.Add(this.cb_tenhang);
            this.panel1.Controls.Add(this.dtp_ngaynhap);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.num_soluong);
            this.panel1.Controls.Add(this.txt_mahang);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txt_ghichu);
            this.panel1.Controls.Add(this.btn_backmain);
            this.panel1.Controls.Add(this.btn_delete);
            this.panel1.Controls.Add(this.lbl_label);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.lbl_productname);
            this.panel1.Controls.Add(this.lbl_quanlity);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(92, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 450);
            this.panel1.TabIndex = 0;
            // 
            // btn_xoatenhang
            // 
            this.btn_xoatenhang.BackColor = System.Drawing.Color.Violet;
            this.btn_xoatenhang.Enabled = false;
            this.btn_xoatenhang.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_xoatenhang.ForeColor = System.Drawing.Color.Blue;
            this.btn_xoatenhang.Image = global::StoreManagement.Properties.Resources.delete_page;
            this.btn_xoatenhang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_xoatenhang.Location = new System.Drawing.Point(849, 81);
            this.btn_xoatenhang.Name = "btn_xoatenhang";
            this.btn_xoatenhang.Size = new System.Drawing.Size(97, 50);
            this.btn_xoatenhang.TabIndex = 162;
            this.btn_xoatenhang.Text = "Xóa";
            this.btn_xoatenhang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_xoatenhang.UseVisualStyleBackColor = false;
            this.btn_xoatenhang.Click += new System.EventHandler(this.btn_xoatenhang_Click);
            // 
            // btn_themtenhang
            // 
            this.btn_themtenhang.BackColor = System.Drawing.Color.Violet;
            this.btn_themtenhang.Enabled = false;
            this.btn_themtenhang.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_themtenhang.ForeColor = System.Drawing.Color.Blue;
            this.btn_themtenhang.Image = global::StoreManagement.Properties.Resources.add_page;
            this.btn_themtenhang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_themtenhang.Location = new System.Drawing.Point(544, 134);
            this.btn_themtenhang.Name = "btn_themtenhang";
            this.btn_themtenhang.Size = new System.Drawing.Size(115, 50);
            this.btn_themtenhang.TabIndex = 161;
            this.btn_themtenhang.Text = "Thêm";
            this.btn_themtenhang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_themtenhang.UseVisualStyleBackColor = false;
            this.btn_themtenhang.Click += new System.EventHandler(this.btn_themtenhang_Click);
            // 
            // lbl_tenhangmoi
            // 
            this.lbl_tenhangmoi.AutoSize = true;
            this.lbl_tenhangmoi.BackColor = System.Drawing.Color.PaleGreen;
            this.lbl_tenhangmoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tenhangmoi.ForeColor = System.Drawing.Color.Blue;
            this.lbl_tenhangmoi.Location = new System.Drawing.Point(15, 145);
            this.lbl_tenhangmoi.Name = "lbl_tenhangmoi";
            this.lbl_tenhangmoi.Size = new System.Drawing.Size(133, 24);
            this.lbl_tenhangmoi.TabIndex = 160;
            this.lbl_tenhangmoi.Text = "Tên hàng mới:";
            // 
            // txt_tenhangmoi
            // 
            this.txt_tenhangmoi.BackColor = System.Drawing.SystemColors.Window;
            this.txt_tenhangmoi.Enabled = false;
            this.txt_tenhangmoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tenhangmoi.Location = new System.Drawing.Point(165, 146);
            this.txt_tenhangmoi.MaxLength = 40;
            this.txt_tenhangmoi.Name = "txt_tenhangmoi";
            this.txt_tenhangmoi.Size = new System.Drawing.Size(351, 26);
            this.txt_tenhangmoi.TabIndex = 159;
            this.txt_tenhangmoi.TextChanged += new System.EventHandler(this.txt_tenhangmoi_TextChanged);
            // 
            // btn_thongke
            // 
            this.btn_thongke.BackColor = System.Drawing.Color.Violet;
            this.btn_thongke.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_thongke.ForeColor = System.Drawing.Color.Blue;
            this.btn_thongke.Image = global::StoreManagement.Properties.Resources.calculator;
            this.btn_thongke.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_thongke.Location = new System.Drawing.Point(165, 366);
            this.btn_thongke.Name = "btn_thongke";
            this.btn_thongke.Size = new System.Drawing.Size(150, 50);
            this.btn_thongke.TabIndex = 157;
            this.btn_thongke.Text = "Thống kê";
            this.btn_thongke.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_thongke.UseVisualStyleBackColor = false;
            this.btn_thongke.Click += new System.EventHandler(this.btn_thongke_Click);
            // 
            // cb_tenhang
            // 
            this.cb_tenhang.AccessibleName = "";
            this.cb_tenhang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_tenhang.BackColor = System.Drawing.Color.LightBlue;
            this.cb_tenhang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tenhang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_tenhang.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_tenhang.Location = new System.Drawing.Point(165, 92);
            this.cb_tenhang.Name = "cb_tenhang";
            this.cb_tenhang.Size = new System.Drawing.Size(666, 32);
            this.cb_tenhang.TabIndex = 156;
            this.cb_tenhang.SelectedIndexChanged += new System.EventHandler(this.cb_tenhang_SelectedIndexChanged);
            // 
            // dtp_ngaynhap
            // 
            this.dtp_ngaynhap.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngaynhap.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtp_ngaynhap.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_ngaynhap.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_ngaynhap.Location = new System.Drawing.Point(165, 194);
            this.dtp_ngaynhap.Name = "dtp_ngaynhap";
            this.dtp_ngaynhap.Size = new System.Drawing.Size(261, 29);
            this.dtp_ngaynhap.TabIndex = 155;
            this.dtp_ngaynhap.Value = new System.DateTime(2020, 5, 8, 17, 5, 30, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.PaleGreen;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(41, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 24);
            this.label3.TabIndex = 154;
            this.label3.Text = "Ngày nhập:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_sanphammoi);
            this.panel2.Controls.Add(this.dgv_sanpham);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 490);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1185, 159);
            this.panel2.TabIndex = 74;
            // 
            // AddProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1185, 649);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddProductForm";
            this.Text = "AddProduct";
            this.Load += new System.EventHandler(this.AddProductForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanphammoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sanpham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productiondataBindingSource7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_soluong)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource productiondataBindingSource1;
        private System.Windows.Forms.BindingSource productiondataBindingSource3;
        private System.Windows.Forms.BindingSource productiondataBindingSource2;
        private System.Windows.Forms.BindingSource productiondataBindingSource6;
        private System.Windows.Forms.BindingSource productiondataBindingSource5;
        private System.Windows.Forms.BindingSource productiondataBindingSource4;
        private System.Windows.Forms.BindingSource productiondataBindingSource;
        private System.Windows.Forms.BindingSource productiondataBindingSource7;
        private System.Windows.Forms.Button btn_backmain;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.DataGridView dgv_sanphammoi;
        private System.Windows.Forms.Label lbl_quanlity;
        private System.Windows.Forms.Label lbl_productname;
        private System.Windows.Forms.Label lbl_danhsach;
        private System.Windows.Forms.Label lbl_label;
        private System.Windows.Forms.TextBox txt_ghichu;
        private System.Windows.Forms.TextBox txt_mahang;
        private System.Windows.Forms.DataGridView dgv_sanpham;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown num_soluong;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtp_ngaynhap;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_tenhang;
        private System.Windows.Forms.Button btn_thongke;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mahang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tenhang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ngaynhap;
        private System.Windows.Forms.DataGridViewTextBoxColumn Giaban;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nhomhang;
        private System.Windows.Forms.Button btn_xoatenhang;
        private System.Windows.Forms.Button btn_themtenhang;
        private System.Windows.Forms.Label lbl_tenhangmoi;
        private System.Windows.Forms.TextBox txt_tenhangmoi;
    }
}