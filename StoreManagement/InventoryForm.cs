﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement
{
    public partial class InventoryForm : Form
    {
        MainForm mMainForm;
        string mUser = "";
        int rowSelected = 0;
        SQLiteDataAdapter _adp = new SQLiteDataAdapter();
        string mDatetimePattern = "dd/MM/yyyy HH:mm:ss";
        string mConnectionString = "Data Source = MyDatabase.sqlite;Version=3;";

        string mStoreQuantity = "";
        public InventoryForm(MainForm mainForm)
        {
            mMainForm = mainForm;
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        private void btn_thongke_Click(object sender, EventArgs e)
        {
            Thongketuychon("", "");
        }

        private void btn_trove_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
            mMainForm.Show();
        }

        private void Thongketuychon(string _mahang, string _tenhang)
        {
            /*
            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {

                c.Open();
                dgv_danhsachhoadon.DataSource = null;
                dgv_danhsachhoadon.Rows.Clear();

                string command = "Select datetime,serial,name,person,storeqtt,tradeqtt,tradetype,note FROM tbl_lending where serial like " + "'" + "%" + _mahang + "%" +
                    "' and DATE(substr(datetime ,1,2)||substr(datetime ,3,2)||substr(datetime ,5,4)) BETWEEN DATE(@p1) AND DATE(@p2)";
                SQLiteDataAdapter  dataAdapter = new SQLiteDataAdapter(command, c);

                SQLiteParameter fromDate = new SQLiteParameter("@p1", SqlDbType.DateTime2);
                fromDate.Value = dtp_tungay.Value;
                SQLiteParameter toDate = new SQLiteParameter("@p2", SqlDbType.DateTime2);
                toDate.Value = dtp_denngay.Value;

                dataAdapter.SelectCommand.Parameters.Add(fromDate);
                dataAdapter.SelectCommand.Parameters.Add(toDate);

                DataTable dt_danhsachhoadon = new DataTable();
                dataAdapter.Fill(dt_danhsachhoadon);

                // Get the DefaultViewManager of a DataTable.
                DataView view = dt_danhsachhoadon.DefaultView;

                // By default, the first column sorted ascending.
                view.Sort = "datetime, serial, name";

                dt_danhsachhoadon.AcceptChanges();
                dgv_danhsachhoadon.DataSource = dt_danhsachhoadon;

                dgv_danhsachhoadon.DataSource = view;

                
            }
            */
            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                dgv_danhsachhoadon.DataSource = null;
                dgv_danhsachhoadon.Rows.Clear();
                int STT_Thongke = 0;
                try
                {
                    DataTable dt = new DataTable();
                    /*
                     _adp = new SQLiteDataAdapter("SELECT datetime,serial,name,person,storeqtt,tradeqtt,tradetype,note FROM tbl_lending WHERE DATE(substr(datetime ,1,2)||substr(datetime ,3,2)||substr(datetime ,5,4)) BETWEEN DATE(@p1) AND DATE(@p2)", c);

                     SQLiteParameter fromDate = new SQLiteParameter("@p1");
                     fromDate.Value = dtp_tungay.Value.ToString("ddMMyyyy");
                     SQLiteParameter toDate = new SQLiteParameter("@p2");
                     toDate.Value = dtp_denngay.Value.ToString("ddMMyyyy");

                     _adp.SelectCommand.Parameters.Add(fromDate);
                     _adp.SelectCommand.Parameters.Add(toDate);

                     */

                    _adp = new SQLiteDataAdapter(string.Format("SELECT datetime,serial,name,person,storeqtt,tradeqtt,tradetype,note FROM tbl_lending WHERE serial LIKE '%{0}%' AND name LIKE '%{1}%' AND substr(datetime,7,4)||'-'||substr(datetime,4,2)||'-'||substr(datetime,1,2) BETWEEN '{2}' AND '{3}'", _mahang,_tenhang, dtp_tungay.Value.ToString("yyyy-MM-dd"), dtp_denngay.Value.ToString("yyyy-MM-dd")), c);
                    _adp.Fill(dt);
                    foreach (DataRow _Datarow in dt.Rows)
                    {
                        STT_Thongke++;
                        string[] arrItem = string.Join(",", _Datarow.ItemArray).Split(',').ToArray();
                        dgv_danhsachhoadon.Rows.Add(STT_Thongke, arrItem[0], arrItem[1], arrItem[2], arrItem[3], arrItem[4], arrItem[5], arrItem[6]=="1"?"Giao":"Nhận", arrItem[7]);
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void txt_mahang_TextChanged(object sender, EventArgs e)
        {
            Thongketuychon(txt_mahang.Text, txt_tenhang.Text);
        }

        private void txt_tenhang_TextChanged(object sender, EventArgs e)
        {
            Thongketuychon(txt_mahang.Text, txt_tenhang.Text);
        }
    }
}
