﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement
{
    public partial class MainForm : Form
    {
        //SQLiteConnection _con = new SQLiteConnection();
        string mConnectionString = "Data Source = MyDatabase.sqlite;Version=3;";

        string _taikhoan = "";
        public MainForm()
        {
            InitializeComponent();
        }

        //private void CreateConnection()
        //{
        //    if (_con.State == ConnectionState.Open)
        //    {
        //        _con.Close();
        //    }
        //    string _strConnect = "Data Source = MyDatabase.sqlite;Version=3;";
        //    _con.ConnectionString = _strConnect;
        //    _con.Open();
        //}

        //public void CloseConnection()
        //{
        //    _con.Close();
        //}

        public void Login()
        {
            if (txt_taikhoan.Text == "")
            {
                MessageBox.Show("Hãy nhập tên người dùng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_taikhoan.Focus();
                return;
            }

            if (txt_matkhau.Text == "")
            {
                MessageBox.Show("Hãy nhập mật khẩu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_matkhau.Focus();
                return;
            }

            try
            {
                //CreateConnection();
                using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
                {
                    c.Open();

                    using (SQLiteCommand sQLiteCommand = new SQLiteCommand("SELECT name, password FROM tbl_user WHERE name = @Username AND password = @Password", c))
                    {
                        SQLiteParameter uName = new SQLiteParameter("@Username");
                        SQLiteParameter uPassword = new SQLiteParameter("@Password");

                        uName.Value = txt_taikhoan.Text;
                        uPassword.Value = txt_matkhau.Text;

                        sQLiteCommand.Parameters.Add(uName);
                        sQLiteCommand.Parameters.Add(uPassword);

                        using (SQLiteDataReader myReader = sQLiteCommand.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (myReader.Read() == true)
                            {
                                lbl_xinchao.Text = string.Format("Xin chào: {0} \nChúc bạn ngày mới tốt lành!", myReader["name"].ToString());
                                _taikhoan = myReader["name"].ToString();

                                btn_thongkegiaonhan.Enabled = true;
                                btn_themsanphammoi.Enabled = true;
                                btn_banhang.Enabled = true;

                                lbl_dangbanquay.Visible = true;
                                lbl_xinchao.Visible = true;
                                lbl_taikhoan.Visible = false;
                                txt_taikhoan.Visible = false;
                                lbl_matkhau.Visible = false;
                                txt_matkhau.Visible = false;

                                btn_dangnhap.Visible = false;
                                btn_dangxuat.Enabled = true;
                                if (_taikhoan == "admin")
                                {
                                    //btnAdd.Visible = true;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Đăng nhập thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                txt_matkhau.Clear();
                                txt_matkhau.Focus();
                            }
                        }
                    }
                }
                btn_themsanphammoi.Focus();
                //CloseConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_dangnhap_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void btn_thoat_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }

        private void btn_dangxuat_Click(object sender, EventArgs e)
        {
            if (_taikhoan == "")
            {
                MessageBox.Show("Bạn hãy đăng nhập tài khoản vào hệ thống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txt_taikhoan.Focus();
            }
            else
            {
                DialogResult result;
                result = MessageBox.Show(string.Format("Bạn có muốn đăng xuất tài khoản: {0} không?", _taikhoan), "Thông báo", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    lbl_xinchao.Visible = false;
                    lbl_taikhoan.Visible = true;
                    txt_taikhoan.Visible = true;
                    lbl_matkhau.Visible = true;
                    txt_matkhau.Visible = true;
                    btn_dangnhap.Visible = true;
                    btn_dangxuat.Enabled = false;
                    btn_thongkegiaonhan.Enabled = false;
                    btn_themsanphammoi.Enabled = false;
                    btn_banhang.Enabled = false;
                    _taikhoan = "";
                    txt_taikhoan.Clear();
                    txt_matkhau.Clear();
                    txt_taikhoan.Focus();
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = txt_taikhoan.Text;
            string pass = txt_matkhau.Text;

            string strInsert = string.Format("INSERT INTO tbl_user(name, password) VALUES('{0}','{1}')", name, pass);

            using (SQLiteConnection c = new SQLiteConnection(mConnectionString))
            {
                c.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(strInsert, c))
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show(string.Format("Add user: {0} success!", name));
                }
            }
        }


        private void txt_taikhoan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (txt_taikhoan.Text == "")
                {
                    MessageBox.Show("Hãy nhập tên người dùng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    txt_matkhau.Focus();
                }
            }

            if (e.KeyCode == Keys.D && e.Control)
            {
                if (btnAdd.Visible)
                    btnAdd.Visible = false;
                else
                    btnAdd.Visible = true;
            }
        }

        private void txt_matkhau_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (txt_matkhau.Text == "")
                {
                    MessageBox.Show("Hãy nhập mật khẩu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Login();
                }
            }
        }

        private void btn_themsanphammoi_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddProductForm _AddProductForm = new AddProductForm(this, _taikhoan);
            _AddProductForm.ShowDialog();
            _AddProductForm.Dispose();
        }

        private void btn_banhang_Click(object sender, EventArgs e)
        {
            this.Hide();
            TradeForm _TradeForm = new TradeForm(this, _taikhoan);
            _TradeForm.ShowDialog();
            _TradeForm.Dispose();
        }


        private void btn_thongkegiaonhan_Click(object sender, EventArgs e)
        {
            this.Hide();
            InventoryForm _InventoryForm = new InventoryForm(this);
            _InventoryForm.ShowDialog();
            _InventoryForm.Dispose();
        }
    }
}
